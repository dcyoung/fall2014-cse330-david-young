<!DOCTYPE html>
<head>
	<title>Delete Files</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    
            $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    if(isset($_POST['file_name'])){
		$_SESSION['file_name'] = trim($_POST['file_name']);
	    }else{
		$_SESSION['file_name'] = "empty_file_name";
	    }
	    $user_directory_address = "/home/dcyoung/User_Accounts/".$_SESSION['username'];
	    //$user_directory_address = ".//User_Accounts".'/'.$_SESSION['username'];
	    //printf("<br>username is: %s <br> filename is: %s <br> user dir address: %s <br>",$_SESSION['username'],$_SESSION['file_name'],$user_directory_address);
        ?>
        
	<h1 class="Big_Bold_Text">Welcome <?php echo  htmlspecialchars($_SESSION['username']); ?> ... to the File Deletion Form</h1>
        <p class="Big_Bold_Text">This will serve as your home for deleting files.</p>
	
      
	<?php
		//$DIRNAME = ".//Test_Dir/".trim($_SESSION['username'])."/";
		$source = $user_directory_address.'/'.$_SESSION['file_name'];
		//printf("<br> Source is: %s <br>", $source);
		if(file_exists ( $source)){
			if(unlink($source)){ //delete it
				printf("The file has been successfully deleted.<br>");
			}else{
				printf("We were unable to delete the file.<br>");
			}
		}else{
			printf("The specified file is not associated with your user account or it does not exists.<br>");
			printf("We were unable to delete the file.");
		}
	?>   
      
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>