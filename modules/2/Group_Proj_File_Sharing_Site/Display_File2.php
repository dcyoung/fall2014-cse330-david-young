<!DOCTYPE html>
<head>
	<title>Display File</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
            if(isset($_POST['file_name'])){
		$_SESSION['file_name'] = trim($_POST['file_name']);
                $file_name_exists = TRUE;
	    }else{
		$file_name_exists = FALSE;
	    }
	    $user_directory_address = "/home/dcyoung/User_Accounts/".$_SESSION['username'];
	    //$user_directory_address = ".//User_Accounts/".trim($_SESSION['username']);
        ?>
        <?php
            if ($file_name_exists){
                $file=$user_directory_address.'/'.$_SESSION['file_name'];
                printf($file);
                if(file_exists ( $file)) {
                    header("Content-type: application/force-download");
                    header("Content-Transfer-Encoding: Binary");
                    header("Content-length: ".filesize($file));
                    //header("Content-disposition: attachment; filename=\"".basename($file)."\"");
                    readfile("$file");
                    printf("File exits <br>");
                }else{
                    printf("No such file exists. <br>");
                }
            }else {
                printf("You didn't enter a filename<br>");
            }
        ?>
        <br>
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>