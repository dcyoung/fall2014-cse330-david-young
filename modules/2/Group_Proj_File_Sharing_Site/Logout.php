<!DOCTYPE html>
<head>
	<title>Logout</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
        <?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    
        ?>
	<h1 class="Big_Bold_Text">Thank you for visiting <?php echo  htmlspecialchars($_SESSION['username']); ?>!</h1>
        
        <p class="Big_Bold_Text">You are now logged out and must log back in before you can access any files.</p>
      
        <?php
            unset($_SESSION['username']);
            session_destroy();
        ?>
        <form action = "index.php" name = "index">
                <input type="submit" value="Login as a different user.">
        </form>
</div></body>
</html>