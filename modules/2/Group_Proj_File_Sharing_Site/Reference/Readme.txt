==============
FileList 1.6.3
==============

Das PHP-Script "FileList" zeigt den Inhalt eines Verzeichnisses in einer Tabelle
an. Neben dem Dateinamen wird auch die Dateigr��e und die Zeit der letzten
�nderung der Datei angezeigt. Die Dateiliste kann nach Namen, Gr��e,
Beschreibung, und nach der �nderungszeit sortiert werden.

Optional k�nnen auch Beschreibungen f�r Dateien und Ordner, basierend auf dem
Dateinamen ausgegeben werden. Dazu muss eine CSV-Tabelle im Verzeichnis
erstellt werden, in dem sich die Dateien befinden. Der Name dieser CSV-Datei
ist mit der Variable "$DescFile" frei w�hlbar. In jeder Zeile der CSV-Tabelle
ist zu Beginn ein regul�rer Ausdruck, der auf die gew�nschten Datei(en)
zutreffen muss. Nach einem Semikolon (;) folgt der Beschreibungstext. Im
Download sind bereits Beschreibungen definiert, die als Beispiel dienen.

Ist im angegebenen Ordner keine Beschreibungsdatei vorhanden, wird im Haupt-
ordner nach dieser Datei gesucht und ggf. diese verwendet.

In der Variable "$BaseDir" wird das Basisverzeichnis bestimmt. Die Vorgabe ist
"files", was dem bereits existierenden Unterverzeichnis, mit einigen Beispiel-
dateien, entspricht.


Verbergen von Ordnern und Dateien
=================================

Da es mitunter unerw�nscht ist, dass bestimmte Dateien in der List ausgegeben
werden, wie etwa PHP-Dateien, k�nnen festgelegte Dateitypen ausgeblendet werden.
Dazu muss der Array in der Variable "$ExcludeFileExts" entsprechend abge�ndert
oder erg�nzt werden.

Dateien und Ordner, welche mit einem Punkt beginnen, werden immer ausgeblendet.

In den beiden Arrays "$ExcludeFolders" und "$ExcludeFiles" k�nnen regul�re
Ausdr�cke festgelegt werden, die die betreffenden Ordner und Dateien verbergen.
Die regul�ren Ausdr�cke m�ssen mit dem Begrenzungszeichen und ggf. mit Optionen
angegeben werden.

Vorgegeben sind bereits alle Ordner und Dateien, welche mit "secret_" oder
"hide_" beginnen.


Professional-Version
====================

Einige Funktionen dieses Programms stehen nur in der Professional-Version zur
Verf�gung (nur f�r ausgew�hlte �bersetzer und sonstige Personen, die das Script
auf andere Weise unterst�tzt haben). F�r eventuelle Fragen wenden Sie sich
bitte an <webmaster@gaijin.at>.

Features der Professional-Version:
  o) Unterordner k�nnen angezeigt werden.
  o) Dateien und Ordner k�nnen mit regul�ren Ausdr�cken ausgeblendet werden.
  o) Eine Navigationsleiste mit Links zu den �bergeordneten Ordnern kann
     angezeigt werden.
  o) Die einzelnen Spalten k�nnen aufsteigend und absteigend sortiert werden.
  o) F�r Dateien und Ordner k�nnen Icons angezeigt werden.
  o) Am Ende der Tabelle kann optional die Anzahl der Ordner und Dateien, sowie
     die Gesamtgr��e der Dateien angezeigt werden.
  o) Am Ende der Tabelle kann optional das Datum und die Zeit der letzten
     Aktualisierung angezeigt werden.
  o) Erweiterte Ausgabe der Dateigr��e: Es werden auch Gigabyte angezeigt und
     Dateien mit weniger als 1 KB k�nnen auch als Byte angezeigt werden.


Changelog
=========

Version 1.6.3:
  [+] Eine Sprachdatei f�r Japanisch wurde hinzugef�gt.
  [+] Eine Sprachdatei f�r Spanisch wurde hinzugef�gt.
  [*] Beim Aufruf des Scripts mit einem nicht existierenden Verzeichnisnamen
      wird jetzt das Hauptverzeichnis angezeigt.

Version 1.6.2:
  [+] Eine Sprachdatei f�r Franz�sisch wurde hinzugef�gt.
  [+] Eine Sprachdatei f�r Italienisch wurde hinzugef�gt.
  [+] Eine Sprachdatei f�r Niederl�ndisch wurde hinzugef�gt.
  [+] Eine Sprachdatei f�r Russisch wurde hinzugef�gt.
  [+] Eine Sprachdatei f�r Schwedisch wurde hinzugef�gt.
  [+] Professional: Erweiterte Ausgabe der Dateigr��e: Es werden auch Gigabyte
      angezeigt und Dateien mit weniger als 1 KB k�nnen auch als Byte angezeigt
      werden.
  [+] Professional: Ordner und Dateien k�nnen jetzt mit regul�ren Ausdr�cken
      versteckt werden.
  [+] Professional: Wenn in einem Ordner keine Beschreibungsdatei vorhanden ist,
      wird jetzt die Beschreibungsdatei des obersten Ordners verwendet.

Version 1.6.1:
  [+] Professional: Das Datum und die Zeit der letzten Aktualisierung kann jetzt
      unter der Tabelle angezeigt werden.
  [+] Eine Sprachdatei f�r Portugiesisch wurde hinzugef�gt.
  [*] Kleinere �nderungen und Korrekturen im Quelltext.

Version 1.6:
  [+] Dateien k�nnen jetzt auch in einem neuen Browser-Tab angezeigt bzw.
      ge�ffnet werden.
  [*] Eine Sprachdatei f�r Estnisch wurde hinzugef�gt.
  [+] Professional: Am Ende der Tabelle kann optional die Anzahl der Ordner und
      Dateien, sowie die Gesamtgr��e der Dateien angezeigt werden.
  [+] Professional: Eine Navigationsleiste mit den Links zu den �bergeordneten
      Ordnern kann jetzt angezeigt werden.
  [*] Die Dateigr��e wird jetzt je nach Gr��e automatisch in Kilobyte oder
      Megabyte angezeigt.
  [*] Diverse kleinere �nderungen im Quellcode.
  [*] F�r Deutsch wurde eine eigene Sprachdatei hinzugef�gt. Damit k�nnen die
      vorgegebenen Texte einfacher ge�ndert werden.

Version 1.5.0.1:
  [+] Eine Sprachdatei f�r Slowakisch wurde hinzugef�gt.
  [*] Kleinere �nderungen im Programmcode und den Sprachdateien.

Version 1.5:
  [+] In den Einstellungen kann der Pfad zur Includedatei "inc_item.php"
      angepasst werden.
  [+] Professional: Jede Spalte kann jetzt aufsteigend und absteigend sortiert
      werden.
  [*] Diverse �nderungen im HTML-Header und an der Tag-Reihenfolge f�r
      W3C-Validit�t.

Version 1.4.1:
  [+] Die Benutzeroberfl�che des Scripts kann jetzt in andere Sprachen �bersetzt
      werden. Die Sprachen Deutsch und Englisch sind bereits verf�gbar.

Version 1.4:
  [+] Professional: Die eckigen Klammen zur Kennzeichnung von Verzeichnissen
      k�nnen nun ausgeblendet werden.
  [+] Professional: F�r Ordner und Dateien k�nnen nun Icons angezeigt werden.
  [*] Diverse �nderungen und Verbesserungen im Quellcode.

Version 1.3:
  [*] Diverse kleinere �nderungen und Korrekturen im Quellcode.
  
Version 1.22:
  [*] Kleinere �nderungen im Quellcode.

Version 1.21:
  [-] Das �nderungsdatum wurde nicht immer richtig sortiert.

Version 1.2:
  [+] Das Feld "Anmerkung" kann jetzt ebenfalls sortiert werden.

Version 1.1:
  [+] Die Dateiliste kann nach Dateinamen, Dateigr��e und nach dem �nderugsdatum
      sortiert werden. Die Vorgabe f�r die Sortierung ist vordefinierbar.
  [+] Mit regul�ren Ausdr�cken k�nnen Beschreibungstexte f�r einzelne Dateien,
      oder Dateigruppen festgelegt werden.

--------------------------------------------------------------------------------
Copyright (c) 2006-2014 Werner Rumpeltesz (www.gaijin.at)
