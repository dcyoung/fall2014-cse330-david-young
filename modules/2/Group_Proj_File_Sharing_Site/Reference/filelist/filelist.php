<?php
// ######################
// ### FileList 1.6.3 ###
// ######################


/* *** LIZENZVERTRAG ***

Pflichten und Einschr�nkungen
-----------------------------

Der Schriftzug "Powered by Gaijin.at", sowie der Link zu "http://www.gaijin.at/"
d�rfen nicht entfernt, ver�ndert oder unkenntlicht gemacht werden und m�ssen gut
sichtbar an unver�nderter Position angezeigt werden.

Der Quellcode des Scripts darf nicht verkauft, oder sonst, kostenlos oder gegen
ein Entgelt weitergegeben, oder in irgendeiner Weise ver�ffentlicht werden.
Dies gilt speziell f�r die Ver�ffentlichung im Internet, auf sog. Heft-CDs oder
anderen Software-Sammlungen.


Benutzungsrechte
----------------

Das Script darf kostenlos f�r private Zwecke genutzt werden. Die Verwendung des
Scripts auf kommerziellen Seiten oder die kommerzielle Verwendung des Scripts
(z.B. durch Webdesigner) ist verboten.

Alle anderen Rechte, einschlie�lich des Ver�ffentlichungsrechts, bleiben beim
Autor.

Es besteht kein Recht auf Support oder sonstige Hilfestellung durch den Autor.

Das Script kann an die pers�nlichen Erfordernisse angepasst werden. Der
Schriftzug "Powered by Gaijin.at", sowie der Link zu "http://www.gaijin.at/"
m�ssen in der unter "Pflichten und Einschr�nkungen" angegebenen Form erhalten
bleiben.

Zuwiderhandlungen gegen Bestimmungen dieses Lizenzvertrages k�nnen
strafrechtlich und zivilrechtlich verfolgt werden.


Haftungsausschluss
------------------

Die Verwendung des Scripts erfolgt auf eigene Verantwortung. Der Autor
�bernimmt keine Haftung f�r die Richtigkeit und Funktionsf�higkeit des Scripts.
Der Autor haftet weder f�r direkte, noch f�r indirekte Sch�den, die durch das
Script entstanden sind. Dies umfasst vor allem, aber nicht ausschlie�lich,
Sch�den an der Hardware, am Betriebssystem oder an anderen Programmen, sowie
die Beeintr�chtigung des Gesch�ftsbetriebes.


Ausnahmen
---------

Die Erteilung einer Ausnahme von den Bestimmungen dieses Lizenzvertrages
erfordert eine ausdr�ckliche Genehmigung des Autors, die ggf. per E-Mail
erteilt wird.

Wenn Sie Fragen zur Lizenz haben, oder eine Ausnahmegenhemigung w�nschen,
senden Sie bitte eine E-Mail an: <info@gaijin.at>.


Professional Version
--------------------

Einige Funktionen dieses Programms stehen nur in der Professional-Version zur
Verf�gung (nur f�r ausgew�hlte �bersetzer und sonstige Personen, die das Script
auf andere Weise unterst�tzt haben). F�r eventuelle Fragen wenden Sie sich
bitte an <webmaster@gaijin.at>.

Features der Professional-Version:
  o) Unterordner k�nnen angezeigt werden.
  o) Dateien und Ordner k�nnen mit regul�ren Ausdr�cken ausgeblendet werden.
  o) Eine Navigationsleiste mit Links zu den �bergeordneten Ordnern kann
     angezeigt werden.
  o) Die einzelnen Spalten k�nnen aufsteigend und absteigend sortiert werden.
  o) F�r Dateien und Ordner k�nnen Icons angezeigt werden.
  o) Am Ende der Tabelle kann optional die Anzahl der Ordner und Dateien, sowie
     die Gesamtgr��e der Dateien angezeigt werden.
  o) Am Ende der Tabelle kann optional das Datum und die Zeit der letzten
     Aktualisierung angezeigt werden.
  o) Erweiterte Ausgabe der Dateigr��e: Es werden auch Gigabyte angezeigt und
     Dateien mit weniger als 1 KB k�nnen auch als Byte angezeigt werden.

*/

define('INTERN_CALL', '1');

// *********************
// *** Einstellungen ***
// *********************

// Directory that contains the files and folders
$BaseDir = 'files';

// Filename of the description file
$DescFile = 'descriptions.csv';

// The following file extensions will be hidden in the file list
$ExcludeFileExts = array('php','inc','html');

// Default column for sorting
// Possible values are:
//   'name' for the filename (ascending)
//   'size' for file size (ascending)
//   'time' for the modification time (descending)
//   'desc' for the description (ascending)
$DefaultSort = 'name';

// Language
include_once('language/english.php');
//include_once('language/german.php');

// Path to the item include file 'inc_item.php'
$IncludeItemFile = 'inc_item.php';

// Show the selected file in a new browser tab
$OpenFileInNewTab = true;

// Number of digits after the comma
$FormatDigitsKB = 2;
$FormatDigitsMB = 2;

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>FileList</title>
<meta http-equiv="content-type" content="text/html; charset=<?php echo GetLngStr("Charset"); ?>">
<link rel=stylesheet type="text/css" href="main.css">
<link rel=stylesheet type="text/css" href="filelist.css">
</head>
<body>

<h1>FileList 1.6.2</h1>
<p>Copyright &copy; 2006-2012 <a href="http://www.gaijin.at/">www.gaijin.at</a></p>
<br>



<!-- *** OWN TEXT (above the file list) *** -->



<?php
  $CurrentDir = trim(GetParam('dir', 'G'));
  $SortMode = substr(trim(GetParam('sort', 'G', $DefaultSort)), 0, 4);
?>
<table class="FileListTable">
<tr>

  <td class="FileListCellTitle">&nbsp;<?php
      if ($SortMode != 'name')
        echo '<b><a href="'.$_SERVER['PHP_SELF'].'?dir='.$CurrentDir.'&amp;sort=name">'.GetLngStr('FileName').'</a></b>';
      else
        echo '<b>'.GetLngStr('FileName').'</b>'; ?>&nbsp;</td>

  <td class="FileListCellTitle" style="text-align:right;">&nbsp;<?php
      if ($SortMode != 'size')
        echo '<b><a href="'.$_SERVER['PHP_SELF'].'?dir='.$CurrentDir.'&amp;sort=size">'.GetLngStr('FileSize').'</a></b>';
      else
        echo '<b>'.GetLngStr('FileSize').'</b>'; ?>&nbsp;</td>

  <td class="FileListCellTitle" style="text-align:center;">&nbsp;<?php
      if ($SortMode != 'time')
        echo '<b><a href="'.$_SERVER['PHP_SELF'].'?dir='.$CurrentDir.'&amp;sort=time">'.GetLngStr('FileModTime').'</a></b>';
      else
        echo '<b>'.GetLngStr('FileModTime').'</b>'; ?>&nbsp;</td>

  <td class="FileListCellTitle" style="text-align:center;">&nbsp;<?php
      if ($SortMode != 'desc')
        echo '<b><a href="'.$_SERVER['PHP_SELF'].'?dir='.$CurrentDir.'&amp;sort=desc">'.GetLngStr('FileComment').'</a></b>';
      else
        echo '<b>'.GetLngStr('FileComment').'</b>'; ?>&nbsp;</td>

</tr>
<?php
  ShowFileList($CurrentDir);
?>
</table>



<!-- *** OWN TEXT (below the file list) *** -->



</body>
</html>

<?php

// ############################################################################

function IsFileExcluded($FileName) {
  global $ExcludeFileExts;

  if (is_dir($FileName)) return False;

  $path_parts = pathinfo($FileName);
  $ext = strtolower($path_parts['extension']);
  return (in_array($ext, $ExcludeFileExts));
}

// ############################################################################

function ShowFileList($CurrentDir) {
  global $BaseDir, $DescFile, $SortMode, $TextNoFiles, $IncludeItemFile, $OpenFileInNewTab;

  // Aktuelles Verzeichnis �berpr�fen
  $CurrentDir = trim(substr($CurrentDir, 0, 209));
  if (substr($CurrentDir, 0, 1) == '.') $CurrentDir = '';
  $path_parts = pathinfo($CurrentDir.'/TEMP');
  $dir = $path_parts['dirname'];
  if (($dir == '/') || ($dir == '\\') || ($dir == '.')) $CurrentDir = '';
  $pos = strpos($CurrentDir, '..');
  if (strlen($pos) > 0) $CurrentDir = '';

  $Directory = $BaseDir;
  if (strlen($CurrentDir) > 0) { $Directory = $BaseDir.$CurrentDir; }

  // Beschreibung lesen
  $d = array();
  $Desc = array();
  if (file_exists($Directory.'/'.$DescFile)) {
    $hFile = @fopen($Directory.'/'.$DescFile, 'r');
    if ($hFile) {
      while (($d = fgetcsv ($hFile, 10240, ';')) != False) {
        array_push($Desc, array($d[0], $d[1]));
      }
    }
    fclose ($hFile);
  }

  // Verzeichnis zum Auslesen �ffnen
  $hDir = @opendir($Directory);
  if (!$hDir) {
    echo '<p><b style="color:#cc0000;">'.GetLngStr('ErrWrongDirName').'</b></p>';
    if ($CurrentDir != '') {
			return ShowFileList('');
		} else {
			return 0;
		}
  }

  $FilesArray = array(array('FileName' => '..', 'IsDir' => True, 'FileSize' => 0, 'FileTime' => 0, 'FileDesc' => ''));
  if ($CurrentDir == '') { array_shift($FilesArray); }

  // Dateien und Verzeichnisse einlesen
  while ($file = readdir ($hDir)) {
    if ( ($file != '.') && ($file != '..') && (substr($file, 0, 1) != '.') &&
         (strtolower($file) != strtolower(substr($DescFile, -(strlen($file))))) &&
         (!IsFileExcluded($Directory.'/'.$file))
       ) {

      $FileDesc = '';
      foreach ($Desc as $d) {
        if (is_dir($Directory.'/'.$file)) {
          if (preg_match('/'.$d[0].'/i', '['.$file.']')) {
            $FileDesc = $d[1];
            break;
          }
        } else {
          if (preg_match('/'.$d[0].'/i', $file)) {
            $FileDesc = $d[1];
            break;
          }
        }
      }
      array_push($FilesArray, array('FileName' => $file,
                                    'IsDir' => is_dir($Directory.'/'.$file),
                                    'FileSize' => filesize($Directory.'/'.$file),
                                    'FileTime' => filemtime($Directory.'/'.$file),
                                    'FileDesc' => $FileDesc
                                    ));
    }
  }

  // Sortierung
  if ($SortMode == 'name') usort($FilesArray, 'Compare_FileName');
  if ($SortMode == 'size') usort($FilesArray, 'Compare_FileSize');
  if ($SortMode == 'time') usort($FilesArray, 'Compare_FileTime');
  if ($SortMode == 'desc') usort($FilesArray, 'Compare_FileDesc');

  // Dateien ausgeben
  foreach($FilesArray as $file) {
    if (!$file['IsDir']) {
      $FileLink = $Directory.'/'.$file['FileName'];
      $FileName = $file['FileName'];
      $FileSize = FormatFileSize($file['FileSize']);
      $FileTime = date('d.m.Y, H:i:s', $file['FileTime']);
      $FileDesc = $file['FileDesc'];
      if ($OpenFileInNewTab) $LinkTarget = ' target="_blank"'; else $LinkTarget = '';
      include($IncludeItemFile);
    }
  }

  if (count($FilesArray) == 0) {
    echo '<tr><td colspan="4" class="FileListCellInfo" style="text-align:center">'.$TextNoFiles.'</td></tr>\n';
  }

  // Das Entfernen oder Unkenntlichmachen des Links zu www.gaijin.at ist ein
  // Versto� gegen das Urheberrecht und die Lizenzbestimmungen.

  // F�r weitere Fragen sowie f�r eine Genehmigung zum Entfernen des Links
  // wenden Sie sich bitte an <info@gaijin.at>

  echo '<tr><td class="FileListInfo" colspan="4" style="text-align:center"><small><b>Powered by <a href="http://www.gaijin.at/">Gaijin.at</a></b></small></td></tr>'."\n";

  // Verzeichnis schlie�en
  @closedir($hDir);
}

// ############################################################################

// Sortierfunktion f�r den Dateinamen (absteigend)
function Compare_FileName ($a, $b) {
  if ($a['FileName'] == $b['FileName']) return 0;
  return ($a['FileName'] < $b['FileName']) ? -1 : 1;
}

// Sortierfunktion f�r die Dateigr��e (absteigend)
function Compare_FileSize ($a, $b) {
  if ($a['FileSize'] == $b['FileSize']) { return Compare_FileName ($a, $b); };
  return ($a['FileSize'] < $b['FileSize']) ? -1 : 1;
}

// Sortierfunktion f�r das �nderungsdatum (aufsteigend)
function Compare_FileTime ($a, $b) {
  if ($a['FileTime'] == $b['FileTime']) { return Compare_FileName ($a, $b); };
  return ($a['FileTime'] > $b['FileTime']) ? -1 : 1;
}

// Sortierfunktion f�r die Beschreibung (absteigend)
function Compare_FileDesc ($a, $b) {
  if ($a['FileDesc'] == $b['FileDesc']) return 0;
  return ($a['FileDesc'] < $b['FileDesc']) ? -1 : 1;
}

// ############################################################################

function GetLngStr($sId, $sParams = '') {
	global $Lang;
    
	if (isset($Lang[$sId]))
		$sResult = $Lang[$sId];
	else
		$sResult = '{Missing string "'.$sId.'"}';

  $aParams = explode("\t", $sParams);
  for ($i = 0; $i < count($aParams); $i++) {
    $sResult = str_replace('%s'.($i + 1).'%', $aParams[$i], $sResult);
  }
  
  return $sResult; 
}

// ############################################################################

function FormatFileSize($iSize) {
	global $FormatDigitsKB, $FormatDigitsMB;
	
	if ($iSize < (1024 * 1000)) {
		return number_format($iSize / 1024, $FormatDigitsKB, ',', '.').' KB';
	} else {
		return number_format($iSize / 1024 / 1024, $FormatDigitsMB, ',', '.').' MB';
	}
}

// ############################################################################

function GetParam($ParamName, $Method = 'P', $DefaultValue = '') {
  if ($Method == 'P') {
    if (isset($_POST[$ParamName])) return $_POST[$ParamName]; else return $DefaultValue;
  } else if ($Method == 'G') {
    if (isset($_GET[$ParamName])) return $_GET[$ParamName]; else return $DefaultValue;
  } else if ($Method == 'S') {
    if (isset($_SERVER[$ParamName])) return $_SERVER[$ParamName]; else return $DefaultValue;
  }
}

// ############################################################################

?>

