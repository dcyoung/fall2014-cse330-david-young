<?php
if (!defined('INTERN_CALL')) die('<p><b>DIRECT ACCESS DENIED!</b></p>');
?>
<tr>
  <td class="FileListCellText" nowrap><a href="<?php echo $FileLink; ?>"<?php echo $LinkTarget; ?>><?php echo $FileName; ?></a></td>
  <td class="FileListCellInfo" nowrap><?php echo $FileSize; ?></td>
  <td class="FileListCellInfo" nowrap><?php echo $FileTime; ?></td>
  <td class="FileListCellDesc"><?php echo $FileDesc; ?></td>
</tr>
