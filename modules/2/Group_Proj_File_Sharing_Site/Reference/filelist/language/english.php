<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "There are no files available at this time.";

$Lang['Folder'] = "Folder";

$Lang['FileName'] = "Filename";
$Lang['FileSize'] = "Size";
$Lang['FileModTime'] = "Modified";
$Lang['FileComment'] = "Comment";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "WRONG DIRECTORY NAME!";

$Lang['BackToParentDir'] = "Back to the parent folder...";

$Lang['TotalFoldersString1'] = "folder";
$Lang['TotalFoldersString'] = "folders";
$Lang['TotalFilesString1'] = "file";
$Lang['TotalFilesString'] = "files";
$Lang['LastUpdateTime'] = "Last Update:"; // Added 1.6.1

$Lang['NavBarTitle'] = "Navigation:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Root folder";

?>