<?php

/*
Eesti keelde t�lkinud Tiit Remmel 16.04.2010
*/

$Lang['Charset'] = "iso-8859-15";
$Lang['TextNoFiles'] = "�htegi faili pole kuvada.";
$Lang['Folder'] = "Kaust";
$Lang['FileName'] = "Failinimi";
$Lang['FileSize'] = "Suurus";
$Lang['FileModTime'] = "Viimati muudetud";
$Lang['FileComment'] = "Kommentaar";
$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";
$Lang['ErrWrongDirName'] = "VALE KAUSTANIMI!";
$Lang['BackToParentDir'] = "Tagasi alamkausta...";
$Lang['TotalFoldersString1'] = "kaust";
$Lang['TotalFoldersString'] = "kausta";
$Lang['TotalFilesString1'] = "fail";
$Lang['TotalFilesString'] = "faili";
$Lang['NavBarTitle'] = "Asukoht:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Juurkaust";
?>