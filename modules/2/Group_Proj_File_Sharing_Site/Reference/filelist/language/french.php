<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "Actuellement pas de fichiers disponibles.";

$Lang['Folder'] = "Dossier";

$Lang['FileName'] = "Nom du fichier";
$Lang['FileSize'] = "Taille";
$Lang['FileModTime'] = "Heure de modification";
$Lang['FileComment'] = "Remarque";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "NOM DE REPERTOIRE ERRON�!";

$Lang['BackToParentDir'] = "Retour au r�pertoire sup�rieur...";

$Lang['TotalFoldersString1'] = "Dossier";
$Lang['TotalFoldersString'] = "Dossier";
$Lang['TotalFilesString1'] = "Fichier";
$Lang['TotalFilesString'] = "Fichiers";

$Lang['NavBarTitle'] = "Navigation:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Dossier de base";

?>