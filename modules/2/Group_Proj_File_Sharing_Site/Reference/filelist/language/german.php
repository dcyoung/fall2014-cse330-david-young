<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "Derzeit sind keine Dateien verf�gbar.";

$Lang['Folder'] = "Ordner";

$Lang['FileName'] = "Dateiname";
$Lang['FileSize'] = "Gr��e";
$Lang['FileModTime'] = "�nderungszeit";
$Lang['FileComment'] = "Anmerkung";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "FALSCHER VERZEICHNISNAME!";

$Lang['BackToParentDir'] = "Zur�ck zum �bergeordneten Verzeichnis...";

$Lang['TotalFoldersString1'] = "Ordner";
$Lang['TotalFoldersString'] = "Ordner";
$Lang['TotalFilesString1'] = "Datei";
$Lang['TotalFilesString'] = "Dateien";
$Lang['LastUpdateTime'] = "Letzte �nderung:"; // Added 1.6.1

$Lang['NavBarTitle'] = "Navigation:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Basisordner";

?>