<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "momentaneamente nessun file disponibile.";

$Lang['Folder'] = "cartella";

$Lang['FileName'] = "nome file";
$Lang['FileSize'] = "grandezza";
$Lang['FileModTime'] = "tempo di modifica";
$Lang['FileComment'] = "annotazione";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "nome file sbagliato!";

$Lang['BackToParentDir'] = "ritorno alla cartella superiore ...";

$Lang['TotalFoldersString1'] = "cartella";
$Lang['TotalFoldersString'] = "cartella";
$Lang['TotalFilesString1'] = "file";
$Lang['TotalFilesString'] = "i file";

$Lang['NavBarTitle'] = "navigazione:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "cartella base";

?>