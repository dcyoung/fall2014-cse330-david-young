<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "今はファイルが使用出来ない。";

$Lang['Folder'] = "フォルダー";

$Lang['FileName'] = "ファイル名";
$Lang['FileSize'] = "サイズ";
$Lang['FileModTime'] = "変更の時間";
$Lang['FileComment'] = "補注";

$Lang['SortCharA'] = "°";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "間違いのディレクトリ名！";

$Lang['BackToParentDir'] = "上位のディレクトリに戻る。。。";

$Lang['TotalFoldersString1'] = "フォルダー";
$Lang['TotalFoldersString'] = "フォルダー";
$Lang['TotalFilesString1'] = "ファイル";
$Lang['TotalFilesString'] = "ファイル";
$Lang['LastUpdateTime'] = "最後の変更："; // Added 1.6.1

$Lang['NavBarTitle'] = "ナビゲーション：";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "ベース・フォルダー";

?>