<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "Er is geen data om weer te geven";

$Lang['Folder'] = "Map";

$Lang['FileName'] = "Filenaam";
$Lang['FileSize'] = "Grootte";
$Lang['FileModTime'] = "gewijzigd";
$Lang['FileComment'] = "Opmerking";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "Onbekend mapnaam!";

$Lang['BackToParentDir'] = "hogerop...";

$Lang['TotalFoldersString1'] = "map";
$Lang['TotalFoldersString'] = "mappen";
$Lang['TotalFilesString1'] = "file";
$Lang['TotalFilesString'] = "files";

$Lang['NavBarTitle'] = "Navigatie:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Rootmap";

?>