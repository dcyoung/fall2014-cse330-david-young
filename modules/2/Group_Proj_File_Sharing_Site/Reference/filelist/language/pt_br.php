<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "N�o existem arquivos dispon�veis no momento.";

$Lang['Folder'] = "Pasta";

$Lang['FileName'] = "Arquivo";
$Lang['FileSize'] = "Tamanho";
$Lang['FileModTime'] = "Modificado";
$Lang['FileComment'] = "Coment�rio";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "NOME DE DIRET�RIO INCORRETO!";

$Lang['BackToParentDir'] = "Voltar para a pasta pai...";

$Lang['TotalFoldersString1'] = "pasta";
$Lang['TotalFoldersString'] = "pastas";
$Lang['TotalFilesString1'] = "arquivo";
$Lang['TotalFilesString'] = "arquivos";
$Lang['LastUpdateTime'] = "�ltima atualiza��o:"; // Added 1.6.1

$Lang['NavBarTitle'] = "Navega��o:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Pasta Ra�z";

?>