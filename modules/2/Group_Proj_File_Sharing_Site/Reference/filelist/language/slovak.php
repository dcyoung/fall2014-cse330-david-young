﻿<?php

$Lang['Charset'] = "utf-8";

$Lang['TextNoFiles'] = "Žiadne nové súbory.";

$Lang['Folder'] = "Priečinok";

$Lang['FileName'] = "Názov súboru";
$Lang['FileSize'] = "Veľkosť";
$Lang['FileModTime'] = "Upravené";
$Lang['FileComment'] = "Komentár";

$Lang['SortCharA'] = "°";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "NESPRÁVNY NÁZOV CESTY!";

$Lang['BackToParentDir'] = "O priečinok vyššie...";

$Lang['TotalFoldersString1'] = "priečinok";
$Lang['TotalFoldersString'] = "priečinky";
$Lang['TotalFilesString1'] = "súbor";
$Lang['TotalFilesString'] = "súbory";
$Lang['LastUpdateTime'] = "Naposledy aktualizované:"; // Added 1.6.1

$Lang['NavBarTitle'] = "Navigácia:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Koreňový priečinok";

?>