<?php

/*
================================================================================
Spanish Language File for PHP FileList 1.6.2
================================================================================
Author:  Alexander Duffner
E-Mail:  <mailto:webmaster@alexanderduffner.de>
Website: https://alexanderduffner.de
================================================================================
*/

/*
NOTES
=====
o) Language files must be encoded as UTF-8 without BOM (Byte Order Mask) if
	 the text contains special characters.
o) No line breaks are allowd before the opening PHP tag or after the closing
	 PHP tag.
*/

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "Actualmente ningunos ficheros son disponibles.";

$Lang['Folder'] = "Clasificador";

$Lang['FileName'] = "Fichero";
$Lang['FileSize'] = "Tama�o";
$Lang['FileModTime'] = "Tiempo de cambio";
$Lang['FileComment'] = "Observaci�n";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "�NOMBRE DE LISTA EQUIVOCADO!";

$Lang['BackToParentDir'] = "Atr�s a la lista superior...";

$Lang['TotalFoldersString1'] = "Clasificador";
$Lang['TotalFoldersString'] = "Clasificador";
$Lang['TotalFilesString1'] = "Fichero";
$Lang['TotalFilesString'] = "Ficheros";
$Lang['LastUpdateTime'] = "Tiempo de cambio:"; // Added 1.6.1

$Lang['NavBarTitle'] = "Navegaci�n:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Clasificador de base";

?>