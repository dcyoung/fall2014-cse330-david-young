<?php

$Lang['Charset'] = "iso-8859-1";

$Lang['TextNoFiles'] = "Det finns inga filer.";

$Lang['Folder'] = "Mapp";

$Lang['FileName'] = "Filnamn";
$Lang['FileSize'] = "Storlek";
$Lang['FileModTime'] = "Senast �ndrad";
$Lang['FileComment'] = "Kommentarer";

$Lang['SortCharA'] = "�";
$Lang['SortCharD'] = "^";

$Lang['ErrWrongDirName'] = "Det gick inte att hitta s�kv�gen!";

$Lang['BackToParentDir'] = "Upp�t...";

$Lang['TotalFoldersString1'] = "Filmapp";
$Lang['TotalFoldersString'] = "Filmapp";
$Lang['TotalFilesString1'] = "Fil";
$Lang['TotalFilesString'] = "Fil";

$Lang['NavBarTitle'] = "Navigation:";
$Lang['NavBarDelim'] = " / ";
$Lang['NavBarRootName'] = "Basismapp";

?>