<!DOCTYPE html>
<head>
	<title>Delete Files</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
        ?>
        
	<h1 class="Big_Bold_Text">Welcome <?php echo  htmlspecialchars($_SESSION['username']); ?> ... to the File Deletion Form</h1>
        <p class="Big_Bold_Text">This will serve as your home for deleting files.</p>
	<br><br>
	<p> Please input the name of the file you would like to delete (including extension).</p>
	<p> For example: "dog_pic.png".</p>
	<br>
	<form action = "Delete_Files.php" name = "file_name" method = "POST">
                File Name: <input type = "text" name = "file_name"><br>
                <input type="submit" value="Delete File">
		<input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form> 
	
	
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>