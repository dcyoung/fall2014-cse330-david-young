<!DOCTYPE html>
<head>
	<title>Share Files</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
        ?>
        
	<h1 class="Big_Bold_Text">Welcome <?php echo  $_SESSION['username']; ?> ... to the File Sharing Form</h1>
        <p class="Big_Bold_Text">This will serve as your home for sharing files with another user.</p>
	<br><br>
	<p> Please input the name of the file (including extension) associated with your account which you would like to share.</p>
	<p> For example: "dog_pic.png".</p>
	<br>
        <p> Please input a valid site username for the recipient of the file.</p>
        
        <form action="Share_Files.php" method="POST">
                File Name: <input type = "text" name = "file_name"><br>
                Recipient: <input type = "text" name = "recipient"><br>
                        
                <input type="submit" value="Share File">
                <input type="hidden" name="username" value="<?php echo  $_SESSION['username']; ?>">
        </form>
        <br><br>
        
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  $_SESSION['username']; ?>">
        </form>
</div></body>
</html>