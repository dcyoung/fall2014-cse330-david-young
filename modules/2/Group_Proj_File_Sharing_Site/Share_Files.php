<!DOCTYPE html>
<head>
	<title>Share Files</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    $_SESSION['file_name'] = filter_var(trim($_POST['file_name']), FILTER_SANITIZE_STRING);
            $_SESSION['recipient'] = filter_var(trim($_POST['recipient']), FILTER_SANITIZE_STRING);
            //$user_directory_address = ".//User_Accounts".'/'.$_SESSION['username'].'/';
	    $user_directory_address = "/home/dcyoung/User_Accounts/".$_SESSION['username'].'/';
            //$recipient_directory_address = ".//User_Accounts".'/'.$_SESSION['recipient'].'/';
	    $recipient_directory_address = "/home/dcyoung/User_Accounts/".$_SESSION['recipient'].'/';
        ?>
        
	<h1 class="Big_Bold_Text">Welcome <?php echo  htmlspecialchars($_SESSION['username']); ?> ... to the File Sharing Page</h1>
        <p>We are currently attempting to share the file:
                <?php echo  htmlspecialchars($_SESSION['file_name']); ?>, with the user <?php echo  htmlspecialchars($_SESSION['recipient']); ?>.</p>
	     
	<?php
                $username_valid = FALSE;    
                //$h = fopen(".//users.txt", "r");
		$h = fopen("/home/dcyoung/User_Accounts/users.txt","r");
                while( !feof($h)){
                    if( trim( fgets($h) ) == trim($_SESSION['recipient']) ){
                        $username_valid = TRUE;
                        break;
                    }
                }
                fclose($h);
                if($username_valid){
                    printf("Recipient Exists!<br>");
                    $source = $user_directory_address.$_SESSION['file_name'];
                    $destination = $recipient_directory_address.$_SESSION['file_name'];
                    //printf("source = %s <br> destination = %s \n",$source,$destination);
                    if(file_exists ( $source)){
			if(file_exists($recipient_directory_address)){
				
				
				if(file_exists($destination)){
				    printf("The recipient already has a file with that name.<br>");
				    printf("We were unable to share your file.<br>");
				}else{
				    if(copy ( $source , $destination )){
					printf("Your file has been successfully shared.<br>");
				    }else{
					printf("We were unable to share your file.<br>");
				    }       
				}
			}else{
				printf("The specified recipient exists, but they have no yet setup their user directory. <br> We were unable to share your file.<br>");
			}
                    }else{
                        printf("The specified file does not exist.<br>");
                        printf("We were unable to share your file.<br>");
                    }
                }else{
                    printf("The specified username for the recipient does not exist.<br>");
                    printf("We were unable to share your file.<br>");
                }
        ?>
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>