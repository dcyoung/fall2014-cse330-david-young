<!DOCTYPE html>
<head>
	<title>View Files</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    //$user_directory_address = ".//User_Accounts/".trim($_SESSION['username']);
	    $user_directory_address = "/home/dcyoung/User_Accounts/".$_SESSION['username'];
        ?>
        <h1 class="Big_Bold_Text">Welcome <?php echo  htmlspecialchars($_SESSION['username']); ?> ... to the File Viewer</h1>
        
        <p class="Big_Bold_Text">This will serve as your home for viewing files.</p>
        <?php
	    //open the directory and grab every file inside, storing each in an array
            $dir_to_list = opendir($user_directory_address);//$address_of_directory_to_list);

            while($fileName = readdir($dir_to_list)) {
                    $Array_of_Files[] = $fileName;
            }
            closedir($dir_to_list);
	            
            $num_of_files = count($Array_of_Files);
	    $num_of_vis_files = $num_of_files-2;
            printf("$num_of_vis_files files, and 2 hidden files<br>\n");
            
	    //sort and print the array of files in a table. 
            sort($Array_of_Files);
            printf("<TABLE border=1 class=whitelinks>\n");
            printf("<TR><TH>Filename</TH><th>Filesize</th><th>Filetype</th></TR>\n");
            // include each file
            for($i=0; $i < $num_of_files; $i++) {
                    if (substr("$Array_of_Files[$i]", 0, 1) != "."){ // don't list hidden files
                            $file_url = $user_directory_address."/".$Array_of_Files[$i];
                            /*
			    printf("<TR><TD><a target = '_blank' href=$file_url>$Array_of_Files[$i]</a></td>");
                            printf("<td>");
                            printf(filesize($user_directory_address."/".$Array_of_Files[$i]));
                            printf("</td>");
                            printf("<td>");
			    printf(filetype($user_directory_address."/".$Array_of_Files[$i]));
                            printf("</td>");
                            printf("</TR>\n");
                            */
			    printf("<TR><TD>$Array_of_Files[$i]</td>");
                            printf("<td>");
                            printf(filesize($user_directory_address."/".$Array_of_Files[$i]));
                            printf("</td>");
                            printf("<td>");
			    printf(filetype($user_directory_address."/".$Array_of_Files[$i]));
                            printf("</td>");
                            printf("</TR>\n");
                    }
            }
            printf("</TABLE>\n");
        ?>
	<br>
		<p><strong> To download or view file, type its full name (including extension) from the list above into the text box.</strong></p>
	<br>
	<form action = "Display_File.php" name = "file_name" method = "POST">
                File To View: <input type = "text" name = "file_name"><br>
                <input type="submit" value="Download File">
		<input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form> 
	<br>	
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>