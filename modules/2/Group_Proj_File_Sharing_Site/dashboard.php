<!DOCTYPE html>
<head>
	<title>Dashboard</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    $user_directory_address = "/home/dcyoung/User_Accounts/".trim($_SESSION['username']);
	    //$user_directory_address = "/home/ParasVora/User_Accounts/".trim($_SESSION['username']);
	    //$user_directory_address = "../../../User_Accounts/".trim($_SESSION['username']);
            //$user_directory_address = ".//User_Accounts/".trim($_SESSION['username']);
            if(!file_exists($user_directory_address)){
                printf("A user account directory does not yet exist for this user.<br>");
                printf("Creating user account directory...<br>");
                if(mkdir($user_directory_address,0775, true)){
                                        printf("Successfully Created User Account Directory <br>");
                }else{
                    printf("Failed to Create User Account Directory <br>");
                }
                
            }
        ?>
        <h1 class="Big_Bold_Text">Welcome <?php echo  htmlspecialchars($_SESSION['username']); ?> ... To Your Personal Dashboard</h1>
        
        <p class="Big_Bold_Text">This will serve as your home for managing files.</p>
	
	<?php
	     //$address_of_directory_to_list = ".//Test_Dir/".trim($_SESSION['username']);
            //open directory
            $dir_to_list = opendir($user_directory_address);//$address_of_directory_to_list);
              
            // get each entry
            while($fileName = readdir($dir_to_list)) {
                    $Array_of_Files[] = $fileName;
            }
            
            // close directory
            closedir($dir_to_list);
            
            //	count elements in array
            $num_of_files = count($Array_of_Files)-2;
	    echo "You have ".$num_of_files." files associated with your account.";
	?>
	
        <form action = "upload_form.php" name = "upload" method = "POST">
                <input type="submit" value="Upload a File">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
        <form action = "View_Files.php" name = "view" method = "POST">
                <input type="submit" value="View a File">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
        <form action = "Select_File_to_Delete.php" name = "delete" method = "POST">
                <input type="submit" value="Delete a File">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
	<form action = "Select_Share_File.php" name = "Share_File" method = "POST">
                <input type="submit" value="Share a File">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
	<br><br>
	<form action = "Logout.php" name = "logout" method = "POST">
                <input type="submit" value="Logout">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>