<!DOCTYPE html>
<head>
	<title>File Share</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<h1 class="Big_Bold_Text">Welcome to the Paras & David File Sharing Website</h1>
        
        <?php
            if (!isset($_SESSION)){
              session_start();
            }
            //$_SESSION['username'] = $_POST['username'];
	    $_SESSION['username'] = filter_var(trim($_POST['username']), FILTER_SANITIZE_STRING);
        ?>
        <?php
            if(isset($_SESSION['username'])){
                $input_username = trim( $_POST['username'] );
                $username_valid = FALSE;    
                //$h = fopen(".//users.txt", "r");
		$h = fopen("/home/dcyoung/User_Accounts/users.txt","r");
                while( !feof($h)){
                        if( trim( fgets($h) ) == $input_username ){
                            $username_valid = TRUE;
                            break;
                        }
                }
                fclose($h);
                if($username_valid){
                    printf("<p><strong> Username Accepted! <br> Loading User Account... </strong></p>\n");
                    
                }else{
                    printf("<p><strong> Login Failed <br> The provided username does not exist. </strong></p>\n");
		    echo '<form action="index.php" name = "index"><input type="submit" value="Try a Different Username"></form>';
                }
            }
        ?>
	
         <form action="dashboard.php" name = "dashboard" method="POST">
		<?php
			if($username_valid)
				echo '<input type="submit" value="Go To DashBoard">';
		?>
		<input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
	</form>
	 
</div></body>
</html>
