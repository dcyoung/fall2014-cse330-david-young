<!DOCTYPE html>
<head>
	<title>Uploading File</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
            
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
	    $user_directory_address = "/home/dcyoung/User_Accounts/".$_SESSION['username'];
            //$user_directory_address = ".//User_Accounts/".trim($_SESSION['username']);
        ?>
        <h1 class="Big_Bold_Text"><?php echo  htmlspecialchars($_SESSION['username']); ?> ... your file is being uploaded.</h1>
        <?php
            $address_of_directory_to_list = $user_directory_address."/";
            $up_file_name = trim($_FILES["file"]["name"]);
            if ( preg_match('/\s/',$up_file_name) ){
                printf("We could not upload your file because the file name ".htmlspecialchars($up_file_name)." contains whitespace. <br>
                       Please replace whitespace with an underscore ( _ ) or similar and try uploading again.");
            }else{
                $temp = explode(".", $up_file_name);
                $file_extension = end($temp);
            
                printf("Selected File for Upload: " . htmlspecialchars($up_file_name) . "<br>");
                printf("File Type: " . htmlspecialchars($_FILES["file"]["type"]) . "<br>");
                printf("File Size: " . htmlspecialchars(($_FILES["file"]["size"]) / 1024) . " kB<br>");
                printf("Temporary file name: " . htmlspecialchars($_FILES["file"]["tmp_name"]) . "<br>");
                if (file_exists($address_of_directory_to_list . trim($_FILES["file"]["name"]))) {
                  printf("A file with the name " . htmlspecialchars($up_file_name) . " is already associated with your account.");
                }else {
                  move_uploaded_file(trim($_FILES["file"]["tmp_name"]),
                  $address_of_directory_to_list . $up_file_name);
                  printf(htmlspecialchars($up_file_name)."  was succesfully uploaded!");
                }
            }
        ?>        
          
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>