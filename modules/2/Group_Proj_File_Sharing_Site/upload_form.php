<!DOCTYPE html>
<head>
	<title>Upload Form</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if (!isset($_SESSION)){
              session_start();
            }
	    $_SESSION['username'] = filter_var($_POST['username'], FILTER_SANITIZE_STRING);
        ?>
        <h1 class="Big_Bold_Text">Welcome <?php echo  htmlspecialchars($_SESSION['username']); ?> ... to the File Upload Form</h1>
        
        <p class="Big_Bold_Text">This will serve as your home for uploading files.</p>

	<form action="upload_file.php" method="post" enctype="multipart/form-data">
		<label for="file">Filename:</label>
		<input type="file" name="file" id="file"><br>
		<input type="submit" name="submit" value="Submit">
		<input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
	</form>

	<br><br>
        <form action = "dashboard.php" name = "dashboard" method = "POST">
                <input type="submit" value="Return to Dashboard">
                <input type="hidden" name="username" value="<?php echo  htmlspecialchars($_SESSION['username']); ?>">
        </form>
</div></body>
</html>