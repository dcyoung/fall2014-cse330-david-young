 <!DOCTYPE html>
<html>
    <body>
        <?php
            $input1 = $_GET["input1"];
            $input2 = $_GET["input2"];
            $numerical_inputs;
            if(is_numeric($input1)&& is_numeric($input2))
                $numberical_inputs = TRUE;
            else
                $numberical_inputs = FALSE;
            
            $operation = $_GET["operation"];
            $result = "NaN";
            $divide_by_zero = FALSE;
            
            if($numberical_inputs ==TRUE){    
                switch ($operation) {
                    case "add":
                        $result = $input1 + $input2;
                        break;
                    case "subtract":
                        $result = $input1 - $input2;
                        break;
                    case "multiply":
                        $result = $input1 * $input2;
                        break;
                    case "divide":
                        if($input2==0){
                            $divide_by_zero = TRUE;
                            break;
                        }
                        $result = $input1 / $input2;
                        break;
                    default:
                        echo "Please input two numerical values and select a single operation";
                        $soundfile = ".//resources/audio/incorrect_ding.wav";
                        echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
                        break;
                }
                if($divide_by_zero){
                        printf("<p><strong> Dividing by 0 does not yield a number </strong></p>\n");
                        $soundfile = ".//resources/audio/incorrect_ding.wav";
                        echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
		}
		else{
                        printf("<p><strong> The calculated result of the operation %s on the
                            inputs %g and %g is...<br> %g </strong></p>\n",
                            htmlentities($operation),
                            htmlentities($input1),
                            htmlentities($input2),
                            htmlentities($result)
                            );
                        $soundfile = ".//resources/audio/correct_ding.wav";
                        echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
		}
            }else{
                    printf("Please provide a numerical value into both input fields.
                           Without one a calculation is not possible.");
                    $soundfile = ".//resources/audio/incorrect_ding.wav";
                    echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
            }
            
            
        ?>
     </body>
</html>