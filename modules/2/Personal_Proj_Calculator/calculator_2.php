<!DOCTYPE html>
<head>
	<title>Calculator</title>
	<link rel="stylesheet" type="text/css" href=".//calculator_style_sheet.css" />
</head>
<body><div id="main">
	<h1 class="Big_Bold_Text">CSE330 Calculator in PHP & HTML</h1>
        <form action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>" method="POST">
                Input #1: <input type = "number" step = "any" name = "input1"><br>
                Input #2: <input type = "number" step = "any" name = "input2"><br>
                                
                <input type="radio" name="operation" value="add">Add<br>
                <input type="radio" name="operation" value="subtract">Subtract<br>
                <input type="radio" name="operation" value="multiply">Multiply<br>
                <input type="radio" name="operation" value="divide">Divide<br>
                        
                <input type="submit" value="Calculate Answer">
        </form>
        
 
	<?php
		if(isset($_POST['operation'])){
								
			$input1 = $_POST['input1'];
                        $input2 = $_POST['input2'];
                        $numerical_inputs;
                        if(is_numeric($input1)&& is_numeric($input2))
                            $numberical_inputs = TRUE;
                        else
                            $numberical_inputs = FALSE;
                        
                        $operation = $_POST['operation'];
                        $result = "NaN";
                        $divide_by_zero = FALSE;
                        
                        if($numberical_inputs ==TRUE){    
                            switch ($operation) {
                                case "add":
					  $result = $input1 + $input2;
					  break;
                                case "subtract":
					  $result = $input1 - $input2;
					  break;
                                case "multiply":
					  $result = $input1 * $input2;
					  break;
                                case "divide":
					  if($input2==0){
					      $divide_by_zero = TRUE;
					      break;
					  }
					  $result = $input1 / $input2;
					  break;
                                default:
					echo "<p><strong> Please input two numerical values and select a
						single operation </strong></p>\n";
					$soundfile = ".//resources/audio/incorrect_ding.wav";
					echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
					break;
				}
				if($divide_by_zero){
					printf("<p><strong> Dividing by 0 does not yield a number </strong></p>\n");
					$soundfile = ".//resources/audio/incorrect_ding.wav";
					echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
				}
				else{
					printf("<p><strong> The calculated result of the operation %s on the
					       inputs %g and %g is...<br> %g </strong></p>\n",
					       htmlentities($operation),
					       htmlentities($input1),
					       htmlentities($input2),
					       htmlentities($result)
					       );
					$soundfile = ".//resources/audio/correct_ding.wav";
					echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
				}
                        }else{
				printf("<p><strong> Please provide a numerical value into both input fields.
				       Without one a calculation is not possible. </strong></p>\n");
				$soundfile = ".//resources/audio/incorrect_ding.wav";
				echo '<EMBED SRC="'.$soundfile.'" HIDDEN="TRUE" AUTOSTART="TRUE"></EMBED>';
                        }
                        
		}   
        ?>
</div></body>
</html>