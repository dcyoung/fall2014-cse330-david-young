<!DOCTYPE html>
	
<head>
	<title>Battlefield Process</title>
	<link rel="stylesheet" type="text/css" href=".//style_sheet.css" />
</head>
<body><div id="main">
	<?php
            if(!isSet($_POST['ammo']) OR !isSet($POST['soldiers'] OR !isSet($_POST['duration']) OR !isSet($POST['critique']){
                printf("You did not provide input for either ammo, soldiers, duration of critique.  Exiting program now.");
                exit();
            }else{
                $ammo = (int) $_POST['ammo'];
                $soldiers = (int) $POST['soldiers'];
                $duration = (double) $_POST['duration'];
                $critique = $POST['critique'];
            }
            
            //access the database... initialize a new mysqli called $mysqli
             
            $mysqli = new mysqli('localhost', 'dcyoung', 'password', 'battlefield');
            if($mysqli->connect_errno) {
                    printf("Connection Failed: %s\n", $mysqli->connect_error);
                    exit;
            }
            
            $stmt = $mysqli->prepare("insert into reports (ammunition, soldiers, duration, critique) values (?, ?, ?, ?)");
            if(!$stmt){
                    printf("Query Prep Failed: %s\n", $mysqli->error);
                    exit;
            }
             
            $stmt->bind_param('ssss', $ammo, $soldiers, $duration, $critique);
             
            $stmt->execute();
            if(!$stmt){
                    printf("Query Execution Failed: %s\n", $mysqli->error);
                    $stmt->close();
                    exit;
            }else{
                $stmt->close();
                printf("Successfully Added a new Report");
                header('Location: battlefield-submit.html');
            }
            
                      
      
        ?>
</div></body>
</html>