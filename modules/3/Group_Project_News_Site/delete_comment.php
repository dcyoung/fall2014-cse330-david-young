<!DOCTYPE html>
	<!--this page serves to delete a comment that was selected on user_comments.php,-->
	<!--only if the user is valid, the comment belongs to them and they were directed-->
	<!--here by the user_comments.php page-->
<head>
	<title>Delete Comment</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
		require "initialize_page.php";
		printf("Successfully initialized page.<br>");
		if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
			printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
			exit;
		}
	?>
	<h1 class="Big_Bold_Text">Delete Comment Page</h1>
        <p class="Big_Bold_Text">We are attempting to delete your comment.</p><br>

		
	<?php
                if(!isSet($_POST['comment_id'])){
                    printf("No comment was specified");
                }else{
		    $safe_comment_id = $mysqli->real_escape_string($_POST['comment_id']);
                    $condition = "DELETE FROM comments WHERE comment_id = ".$safe_comment_id;
                    $result = mysqli_query($mysqli,$condition);
                    if($result){
                        printf("Comment Successfully Deleted");
                    }else{
                        print("Failed to Delete Comment");
                    }
                    mysqli_close($mysqli);
                }
                
	?>
	<!--provide a return to the main news feed-->
	<form action = "news_feed.php" name = "return" method = "POST">
                <input type="submit" value="Return to News Feed">
                <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
        </form><br>
</div></body>
</html>