<!DOCTYPE html>
	<!--this page serves to delete a post that was selected on user_posts.php,-->
	<!--only if the user is valid, the post belongs to them and they were directed-->
	<!--here by the user_posts.php page-->
<head>
	<title>Delete Post</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
		require "initialize_page.php";
		printf("Successfully initialized page.<br>");
                if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
			printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
			exit;
		}
	?>
	<h1 class="Big_Bold_Text">Delete Post Page</h1>
        <p class="Big_Bold_Text">We are attempting to delete your post.</p><br>

		
	<?php
                if(!isSet($_POST['post_id'])){
                    printf("No post was specified");
                }else{
		    $safe_post_id = $mysqli->real_escape_string($_POST['post_id']);	
                    $condition = "DELETE FROM posts WHERE post_id = ".$safe_post_id;
                    $result = mysqli_query ($mysqli,$condition);
                    if($result){
                        printf("Post Successfully Deleted");
                    }else{
                        print("Failed to Delete Post");
                    }
                    $condition = "DELETE FROM comments WHERE associated_post_id = ".$safe_post_id;
                    $result = mysqli_query ($mysqli,$condition);
                    if($result){
                        printf("<br>Comments Associated with the Post were Successfully Deleted...<br>");
                    }else{
                        printf("Failed to Delete Comments Associated with the Post<br>");
                    }
                    mysqli_close($mysqli);
                }
                
	?>
	
	<form action = "news_feed.php" name = "return" method = "POST">
                <input type="submit" value="Return to News Feed">
                <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
        </form><br>
</div></body>
</html>