<!DOCTYPE html>
	<!--this page serves to edit a comment that was selected on user_comments.php,-->
	<!--only if the user is valid, the comment belongs to them and they were directed-->
	<!--here by the user_comments.php page-->
<head>
	<title>Edit Comment</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
		require "initialize_page.php";
		printf("Successfully initialized page.<br>");
		if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
			printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
			exit;
		}
	?>
	<h1 class="Big_Bold_Text">Edit Comment Page</h1>
        <p class="Big_Bold_Text">Here you may edit your Comment.</p><br>

		
	<?php
                if(!isSet($_POST['comment_id'])){
                    printf("No comment was specified");
		    exit;
                }                
	?>
	<form action="submit_edit_comment.php" method="post">
		<textarea rows="10" cols="50" placeholder="Enter your edited comment here" name="comment_entry" id="comment_entry" required></textarea><br>
		<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>">
		<input type="hidden" name="comment_id" value="<?php echo $_POST['comment_id'];?>"> 
		<input type="submit" value="Submit!">
	</form><br>
	<form action = "news_feed.php" name = "return" method = "POST">
                <input type="submit" value="Return to News Feed">
                <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
        </form><br>
</div></body>
</html>