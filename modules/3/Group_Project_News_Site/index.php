<!DOCTYPE html>
	<!--this page serves as the initial login and redirect page for any improper page load.-->
	<!--It creates a session, destroying after destroying any previous sessions, and permits-->
	<!--directions to login, register new users or sign in as a guest.-->
<head>
	<title>NEWS LOGIN</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<h1 class="Big_Bold_Text">Welcome to the Paras & David NEWS FEED</h1>
        
        <p class="Big_Bold_Text">Please Enter Your Username and Password to Login</p>
        <?php
		
            if (!isset($_SESSION)){
		session_start();
            }else{
		session_destroy();
		session_start();
	    }

        ?>
        <form action = "login.php" name = "login" method = "POST">
                Username: <input type = "text" name = "username"><br>
		Password: <input type = "password" name = "password"><br>
                <input type="submit" value="Login">
        </form>
        <br>
	
	<p>If you simply wish to view the News Feed without posting or commenting, you may log in as a guest below.</p>
	 <form action = "login.php" name = "login_as_guest" method = "POST">
                <input type="submit" value="Login as Guest">
        </form>
	<br>
	<p>If you don't have a username, you can register as a new user below.</p>
	<form action = "register.php" name = "Register">
           <input type="submit" value="New User Registration Form">
        </form>
</div></body>
</html>