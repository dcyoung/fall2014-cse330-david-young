<?php
    //this page serves to initialize every other page.  It will be required in all pages.  It checks to make sure
    //that a token was properly passed from the page that directed you to the new page, that the username is valid and if not
    //then it redirects you to the index or makes you a visitor depending on different conditions
    require "database.php";
    session_start();
    
    if(!isSet($_POST['token'])){
        printf("You must have come to this page directly (not through the login page).<br>");
	printf("Please return to a login page and then find your way here, via a natural path.");
	header('Location: index.php');
	//printf("<form action = \"index.php\" name = \"index\">");
	//printf("<input type=\"submit\" value=\"Return to Login Page\">");
	//printf("</form>");
	exit;
//        printf("You are being redirected to your current location, but as a visitor.<br>");
//        $_SESSION['username'] = "visitor";
//        $_SESSION['user_id'] = 1;
            
    }elseif($_SESSION['token'] !== $_POST['token']){
        printf("Your CSRF token did not match, you are quite nefarious.<br>");
	die("Request forgery detected");
    }else if (!isSet($_SESSION['username']) or !isSet($_SESSION['user_id'])){
        printf("You username or user_id was not specified.<br> You are being redirected to your current location, but as a visitor.<br>");
        $_SESSION['username'] = "visitor";
        $_SESSION['user_id'] = 1;
    }else{
        //do nothing		
    }
		
?>