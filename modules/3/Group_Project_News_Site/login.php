<!DOCTYPE html>
	<!--this page serves as the actual login after credentials were entered in index.php... it will check the-->
	<!--database to verify that the username and password are valid and if not than redirect as a visitor.-->
<head>
	<title>NEWS LOGIN</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">

<?php

    require "database.php";
     if (!isset($_SESSION)){
	session_start();
    }else{
	session_destroy();
	session_start();
    }
    
    if (isset($_POST['username']) and isset($_POST['password'])) {
        $_SESSION['username'] = filter_var(trim($_POST['username']),FILTER_SANITIZE_STRING);
        $_SESSION['password'] = filter_var(trim($_POST['password']),FILTER_SANITIZE_STRING);
        $just_visiting = FALSE;
    }else{
        /* need to handle this here... we want to push them through but without a login */
        $_SESSION['user_id'] = 1;/*make a default user_id here "1" that is the user_id for a visitor*/
	$_SESSION['username'] = "visitor";
        $just_visiting = TRUE;
        printf("You are just a visitor, you cannot post... but have fun reading the news!");
    }
    
    if(!$just_visiting){
        $stmt = $mysqli->prepare("SELECT COUNT(*), id, encrypted_pw FROM users WHERE user_name=?");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}
        
	$stmt->bind_param('s', $_SESSION['username']);
	$stmt->execute();
        
        $stmt->bind_result($num_returns, $user_id, $encrypted_pw);
	$stmt->fetch();
        
        if( $num_returns == 1 && crypt($_SESSION['password'], $encrypted_pw)==$encrypted_pw){
            //Successful Login
            printf("Login Successful");
            $_SESSION['user_id'] = $user_id;
        }
        else{
            printf("Login Attempt Failed. Something was wrong with your login credentials.<br>Please hit back in your browser to try again.");
            exit;
        }
	          
    }
    //Create a CSRF token
    $_SESSION['token'] = substr(md5(rand()),0,10); //generate 10 characer random string
    
    
    
?>
      <form action = "news_feed.php" name = "login" method = "POST">
            <input type="submit" value="Go to News Feed">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
      </form>
</div></body>
</html>