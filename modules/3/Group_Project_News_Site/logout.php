<!DOCTYPE html>
	<!--this page serves as a logout for the user... it will unset any session variables and destroy the session-->
	<!--before providing a redirect to the index.php-->
<head>
	<title>Logout</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
        <?php
	    require "initialize_page.php";
            
        ?>
	<h1 class="Big_Bold_Text">Thank you for visiting <?php echo  htmlspecialchars($_SESSION['username']); ?>!</h1>
        
        <p>You are now logged out and must log back in before you edit or delete posts and comments.</p>
      
        <?php
            unset($_SESSION['username']);
	    unset($_SESSION['user_id']);
	    unset($_SESSION['token']);
            session_destroy();
        ?>
        <form action = "index.php" name = "index">
                <input type="submit" value="Return to Login Screen">
        </form>
</div></body>
</html>