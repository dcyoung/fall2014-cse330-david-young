<!DOCTYPE html>
	<!--this page serves as the main news feed.  it provides links to other important pages and also-->
	<!--displays all the posts made in descending chronological order-->
<head>
	<title>NEWS Feed</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
		require "initialize_page.php";
		printf("Successfully initialized page.<br>");
	?>
	<form action = "logout.php" name = "logout" method = "POST">
            <input type="submit" value="LOGOUT">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
	</form>
	<h1 class="Big_Bold_Text">Welcome to the Paras & David NEWS FEED</h1>
        <p class="Big_Bold_Text">This is the home for news.</p><br>
	
	<form action = "search_entry.php" name = "new_search" method = "POST">
            <input type="submit" value="Search the Site">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
	</form>
	<form action = "user_posts.php" name = "view_users_posts" method = "POST">
            <input type="submit" value="View and Edit My Posts">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
	</form>
	<form action = "user_comments.php" name = "view_users_comments" method = "POST">
            <input type="submit" value="View and Edit My Comments">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
	</form>
	<br>
	<form action = "post_entry.php" name = "new_post" method = "POST">
            <input type="submit" value="Make a New Post">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
	</form>
	<br>
	<?php
		$result = mysqli_query ($mysqli,"SELECT * FROM posts ORDER BY post_timestamp DESC");
		while($row = mysqli_fetch_array($result)) {
			echo "<a href = \"".$row['associated_link']."\"> ".$row['associated_link']. "</a><br>";
			echo $row['post_content'];
			echo "<br>";
			printf("<form action = \"view_comments.php\" name = \"comments\" method = \"POST\">");
			printf("<input type=\"submit\" value=\"View Add Comments\">");
			printf("<input type = \"hidden\" name = \"post_id\" value = ".$row['post_id']." >");
			printf("<input type = \"hidden\" name = \"token\" value = %s>",$_SESSION['token']);
			printf("</form>");
			echo "<br><br>";
			
		}
		mysqli_close($mysqli);
	
	?>
	
	<br><br>
      
       
</div></body>
</html>