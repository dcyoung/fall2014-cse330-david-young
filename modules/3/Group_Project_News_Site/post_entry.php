<!DOCTYPE html>
	<!--this page serves as a field to enter the post information for a new post... it does not handle injection into the database-->
<html>
	<head>
		<title>
			Create a Post
		</title>
		<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css">
	</head>
	<body>
		<div id="main">
			<h1 class="Big_Bold_Text">
				Welcome to the New Post Form.
			</h1>
			<p class="Big_Bold_Text">
				Submit a post here! 
			</p>
			<?php
				require "initialize_page.php";
			?>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
			<?php
				if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
					printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
					exit;
				}
			?>
			<form action="submit_post.php" method="post">
				<input type="url" placeholder="Paste a link here" name="post_link" id="post_link" required><br>
				<textarea rows="10" cols="50" placeholder="Enter your post here" name="post_entry" id="post_entry" required></textarea><br>
				<input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>"> 
				<input type="submit" value="Submit!">
			</form><br>
			<br>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
		</div>
	</body>
</html>