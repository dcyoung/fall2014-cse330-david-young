<!DOCTYPE html>
	<!--this page serves a new user registration form, it will not validate the entry, just take it and feed it to the validation page-->
<head>
	<title>Register New User</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<h1 class="Big_Bold_Text">Welcome to the New User Registration Form</h1>
        <br>
        <p class="Big_Bold_Text">Please Enter a desired Username and Password To Register</p>
        <br>
        
        <form action="validate_new_user.php" method="POST">
            Desired Username: <input type="text" name="desired_username"><br>
            Desired Password: <input type="text" name="desired_password">
            <input type="submit" value="Register">
        </form>
        <br>
</div></body>
</html>