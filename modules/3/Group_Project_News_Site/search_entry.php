<!DOCTYPE html>
	<!--this page serves as the creative portion of the project.  it permits entire database searches for a term in a comment or a post-->
	<!--just like a normal forum board. The user may select between searching for the term in comments or posts and provide the term-->
<html>
	<head>
		<title>
			Site Search
		</title>
		<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css">
	</head>
	<body>
		<div id="main">
			
			<h1 class="Big_Bold_Text">
				Welcome to the Search Page.
			</h1>
			<p class="Big_Bold_Text">
				Search the site for posts or comments here! 
			</p>
			<?php
				require "initialize_page.php";
			?>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
			<?php
				if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
					printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
					exit;
				}
			?>
			
			
			<form action="submit_search.php" method="post">
				Search Term: <input type = "text" name = "search_term" required><br>
				<input type="radio" name="search_pool" value="posts">Search in Posts<br>
				<input type="radio" name="search_pool" value="comments">Search in Comments<br>
                                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>"> 
				<input type="submit" value="Search!">
			</form><br>
			
			<br>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
		</div>
	</body>
</html>