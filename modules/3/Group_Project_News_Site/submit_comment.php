<!DOCTYPE html>
	<!--this page serves to actually inject the provided comment into the database, provided all credentials check out. -->
<html>
	<head>
		<title>
			Create a Comment
		</title>
		<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css">
	</head>
	<body>
		<div id="main">
			<h1 class="Big_Bold_Text">Welcome to the Commentary Section</h1>
			<p>This is home for all comments related to the selected post.</p><br>
			<?php
				require "initialize_page.php";
				if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
					printf("I'm sorry. Visitors can only view the news feed. Additional features are for registered user only.");
				}else{
					if (isset($_POST['comment_entry'])) {
						$stmt = $mysqli->prepare("insert into comments (comment_content, commentor_id, associated_post_id) values (?, ?, ?)");
						if(!$stmt){
							printf("Couldn't prepare query: %s\n", $mysqli->error);
							exit;
						}
					$stmt->bind_param('sii', $_POST['comment_entry'],$_SESSION['user_id'],$_POST['associated_post_id']);
					$stmt->execute();
					$stmt->close();
					}
					printf("<br>Successfully submitted your comment.<br>");
				}
			?>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
		</div>
	</body>
</html>