<!DOCTYPE html>
	<!--this page serves to actually alter the database entry for a submitted post, provided an updated entry-->
<head>
	<title>Edit Post</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
		require "initialize_page.php";
		printf("Successfully initialized page.<br>");
		if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
			printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
			exit;
		}
	?>
	<h1 class="Big_Bold_Text">Edit Post Page</h1>
        <p class="Big_Bold_Text">Here you may edit your post.</p><br>
	
		
	<?php
                if(!isSet($_POST['post_id'])){
                    printf("No post was specified");
                }else{
		    $safe_post_entry = $mysqli->real_escape_string($_POST['post_entry']);
		    $condition = "UPDATE posts SET post_content = '" . $safe_post_entry . "' WHERE post_id = " . $_POST['post_id'];
                    $result = mysqli_query($mysqli,$condition);
                    if($result){
                        printf("Successfully Updated your Post<br>");
                    }else{
                        printf("Failed to Update your Post<br>");
                    }
                    mysqli_close($mysqli);
                }
                
	?>
	
	<form action = "news_feed.php" name = "return" method = "POST">
                <input type="submit" value="Return to News Feed">
                <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
        </form><br>
</div></body>
</html>