<!DOCTYPE html>
	<!--this page serves to actually inject an entry into the database entry for a submitted post, provided an entry-->
<html>
	<head>
		<title>
			Create a Post
		</title>
		<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css">
	</head>
	<body>
		<div id="main">
			<h1 class="Big_Bold_Text">
				Welcome to the Paras and David NEWS FEED
			</h1>
			<p class="Big_Bold_Text">
				Submit a post here! 
			</p>
			<?php
				require "initialize_page.php";
				if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
					printf("I'm sorry. Visitors can only view the news feed. Additional features are for registered user only.");
				}else{
					if (isset($_POST['post_entry'])) {
						$stmt = $mysqli->prepare("insert into posts (post_content, associated_link, poster_id) values (?, ?, ?)");
						if(!$stmt){
							printf("Couldn't prepare query: %s\n", $mysqli->error);
							exit;
						}
					$stmt->bind_param('ssi', $_POST['post_entry'],$_POST['post_link'],$_SESSION['user_id']);
					$stmt->execute();
					$stmt->close();
					}
					printf("<br>You successfully submitted a new post!<br>");
				}
				
			?>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
		</div>
	</body>
</html>