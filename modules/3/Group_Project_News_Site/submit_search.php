<!DOCTYPE html>
	<!--this page serves to actually search the database for the search criteria and then return the results to the user.-->
<html>
	<head>
		<title>
			Search Results
		</title>
		<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css">
	</head>
	<body>
		<div id="main">
			<h1 class="Big_Bold_Text">
				Search Results.
			</h1>
			<p class="Big_Bold_Text">
				The results of your search can be seen below.
			</p>
			<?php
				require "initialize_page.php";
				
				if(!isSet($_POST['search_term']) or !isSet($_POST['search_pool'])){
					printf("Missing search term or search criteria.");
					$valid_search = FALSE;	
				}else{
					$valid_search = TRUE;
				}
				
				if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
					printf("I'm sorry. Visitors can only view the news feed. Additional features are for registered user only.");
				}else if($valid_search){
					$count = 0;
					$safe_search_term = $mysqli->real_escape_string($_POST['search_term']);
					if($_POST['search_pool'] =='comments'){
						$condition = "SELECT * FROM comments WHERE comment_content LIKE '%".$safe_search_term . "%' ORDER BY comment_timestamp ASC";
						$result = mysqli_query ($mysqli,$condition);
						while($row = mysqli_fetch_array($result)) {
							echo "User ".$row['commentor_id'] . " posted the following at " . $row['comment_timestamp']."<br>";
							echo $row['comment_content'];
							echo "<br><br>";
							$count = $count+1;	
						}
					}else{
						$condition = "SELECT * FROM posts WHERE post_content LIKE '%".$safe_search_term . "%' ORDER BY post_timestamp ASC";
						$result = mysqli_query ($mysqli,$condition);
						while($row = mysqli_fetch_array($result)) {
							echo "User ".$row['poster_id'] . " posted the following at " . $row['post_timestamp']."<br>";
							echo $row['post_content'];
							echo "<br><br>";
							$count = $count+1;
						}
					}
					mysqli_close($mysqli);
					printf("Your search returned ".$count." results.");
				}else{
					printf("The search was invalid.<br><br>");
				}
			?>
			
			<br>
			<form action = "news_feed.php" name = "return" method = "POST">
				<input type="submit" value="Return to News Feed">
				<input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
			</form><br>
		</div>
	</body>
</html>