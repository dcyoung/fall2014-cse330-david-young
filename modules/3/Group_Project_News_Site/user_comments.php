<!DOCTYPE html>
	<!--this page serves to show any user their history of comments, permitting them options to delete or edit any comment-->
<head>
	<title>NEWS Feed</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php		
		require "initialize_page.php";
	?>
	<form action = "news_feed.php" name = "return" method = "POST">
                <input type="submit" value="Return to News Feed">
                <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
        </form><br>
	<?php
		printf("Successfully initialized page.<br>");
                if(($_SESSION['username']=='visitor') or ($_SESSION['user_id']==1)){
			printf("Im sorry. Visitors can only view the news feed. Additional features are for registered user only.");
			exit;
		}
	?>
	<h1 class="Big_Bold_Text">Welcome to the Your User Comment History</h1>
        <p class="Big_Bold_Text">From here you may edit your previous comments.</p><br>

		
	<?php
		
                $condition = "SELECT * FROM comments WHERE commentor_id = ".$_SESSION['user_id']. " ORDER BY comment_timestamp ASC";
		$result = mysqli_query ($mysqli,$condition);
		while($row = mysqli_fetch_array($result)) {
			echo "Comment ". $row['comment_id'] . "	made by user with user_id:" . $row['commentor_id']."<br>";
			echo $row['comment_content'];
			printf("<form action = \"delete_comment.php\" name = \"delete_comment\" method = \"POST\">");
			printf("<input type=\"submit\" value=\"Delete This Comment\">");
			printf("<input type = \"hidden\" name = \"comment_id\" value = ".$row['comment_id']." >");
			printf("<input type = \"hidden\" name = \"token\" value = %s>",$_SESSION['token']);
			printf("</form>");
                        printf("<form action = \"edit_comment.php\" name = \"edit_comment\" method = \"POST\">");
			printf("<input type=\"submit\" value=\"Edit This Comment\">");
			printf("<input type = \"hidden\" name = \"comment_id\" value = ".$row['comment_id']." >");
			printf("<input type = \"hidden\" name = \"token\" value = %s>",$_SESSION['token']);
			printf("</form>");
			echo "<br><br>";
			
		}
		mysqli_close($mysqli);
	
	?>
	
	<form action = "news_feed.php" name = "return" method = "POST">
                <input type="submit" value="Return to News Feed">
                <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
        </form><br>
</div></body>
</html>