<!DOCTYPE html>
	<!--this page serves to validate the attempted registration of a new user.  It checks the username doesn't yet exist and then-->
	<!--creates it, and injects it into the database safely if it doesn't.-->
<head>
	<title>Validating New User</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<h1 class="Big_Bold_Text">Validating New User</h1>
        <br>
        <?php
            require "database.php";
            session_start();
            if (isset($_POST['desired_username']) and isset($_POST['desired_password'])) {
                    $desired_username = $_POST['desired_username'];
                    $desired_password = $_POST['desired_password'];
            }
            else {
                    printf("Either the desired username or password field was not set.");
                    printf("Cannot register a new user.");
                     header('Location: index.php');
                    exit;
            }
           
           
           $where_condition =  "user_name = " + $desired_username;
           if(userExists("users",$where_condition,$mysqli)){
               printf("The desired username is already taken.");
               exit;
           }
           else{
                $stmt = $mysqli->prepare("insert into users (user_name, encrypted_pw) values (?, ?)");
                if(!$stmt){
                        printf("Query Prep Failed: %s\n", $mysqli->error);
                        exit;
                }
                 
                $stmt->bind_param('ss', $desired_username, crypt($desired_password));
                 
                $stmt->execute();
                 
                $stmt->close();
                printf("Successfully Registered a New User, Please Return to the Login Page to Login");
           }
           
       
           function userExists($table, $where, $mysqli_instance) {
               $query = "SELECT * FROM $table WHERE $where";
               $result = $mysqli_instance->query($query);
 
               if($result->num_rows > 0) {
                    printf("Number of rows = %i",$result->num_rows);
                   return true; 
               }
               return false; 
            } 
            
        ?>
        
        <form action = "index.php" name = "Login">
                <input type="submit" value="Return to Login Page">
        </form>

        
</div></body>
</html>