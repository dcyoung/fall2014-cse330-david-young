<!DOCTYPE html>
	<!--this page serves to show all the comments regarding a specific post.  It also has a text box to enter a new comment in response.-->
<head>
	<title>Comment Feed</title>
	<link rel="stylesheet" type="text/css" href=".//login_style_sheet.css" />
</head>
<body><div id="main">
	<?php
		require "initialize_page.php";
		printf("Successfully initialized page.<br>");
	?>
	<h1 class="Big_Bold_Text">Welcome to the Commentary Section</h1>
        <p class="Big_Bold_Text">This is home for all comments related to the selected post.</p><br>
	
        
	<?php
                $condition = "SELECT * FROM comments WHERE associated_post_id = " . $_POST['post_id'] . " ORDER BY comment_timestamp ASC";
		$result = mysqli_query ($mysqli,$condition);
		while($row = mysqli_fetch_array($result)) {
			echo "User ".$row['commentor_id'] . " posted the following at " . $row['comment_timestamp']."<br>";
			echo $row['comment_content'];
			echo "<br><br>";
			
		}
		mysqli_close($mysqli);
	
	?>
        
        <br>
        <form action="submit_comment.php" method="post">
                <br>
                <textarea rows="10" cols="50" placeholder="Enter your comment here" name="comment_entry" id="comment_entry" required></textarea><br>
                <input type="hidden" name="token" value="<?php echo $_SESSION['token'];?>">
                <input type="hidden" name="associated_post_id" value="<?php echo $_POST['post_id'];?>"> 
                <input type="submit" value="Submit!">
        </form><br>
        
       <form action = "news_feed.php" name = "return" method = "POST">
            <input type="submit" value="Return to News Feed">
            <input type = "hidden" name = "token" value = "<?php echo $_SESSION['token'];?>"/>
      </form>
</div></body>
</html>