import sys, os, re

class parser:
    # constructor:
    def __init__(self, filename):
        self.file = filename
        self.players = []
        self.num = 2
        self.game_title_regex = re.compile(r"^[=]{3}")
        self.player_name_regex = re.compile(r"(?P<player>[\w\s]+)\sbatted")
                
                
    def add_player(self, player_name):
        self.players.append(player(player_name))
        
    def assign_player(self,name):
        player_exists = False
        player_index = len(self.players)
        for i in range(len(self.players)):
            if self.players[i].name == name:
                player_exists = True
                player_index = i
        if player_exists == True:
            return self.players[player_index]
        else:
            self.add_player(name)
            return self.players[player_index]
        
        
    def parse_game_title(self, input_line):
        match = self.game_title_regex.match(input_line)
        if match is not None:
            return True
        else:
            return False
                
    def parse_player_name(self,input_line):
        match = self.player_name_regex.match(input_line)
        if match is not None:
            return match.group(1)
        else:
            return False
                
    def parse_player_stats(self, input_line):
        p_game_stats = re.findall(r'\d+', input_line)  
        if p_game_stats is not None:
            return p_game_stats
        else:
            return False
    
    def calculate_all_batting_averages(self):
        for player in self.players:
            b_avg = float(player.hits)/float(player.bats)
            player.set_batting_avg(b_avg)
            
    def get_players(self):
        return self.players
        
    def parse(self):
      
        s = ""
        f = open(self.file)
        for line in f:
            s = line.rstrip()
            if self.parse_game_title(s) == False:                        #check to make sure the line is not a game title
                                            
                player_name = self.parse_player_name(s)              #get the players name
                if player_name !=False:
                    player_name = player_name.strip()
                    p = self.assign_player(player_name);         #add the player if they don't exist yet, and make them the current player
                    p_game_stats = self.parse_player_stats(s)    #get the player stats
                    if p_game_stats !=False:                                   
                        p.add_bat(int(p_game_stats[0]))      #add the players bats
                        p.add_hit(int(p_game_stats[1]))      #add the players hits
                        p.add_run(int(p_game_stats[2]))      #add the players runs
                                                        
        f.close()    
        self.calculate_all_batting_averages()
            
            
class player:
	# constructor:
    def __init__(self, name):
        self.name = name
        self.runs = 0
        self.bats = 0
        self.hits = 0
        self.runs = 0
        self.batting_avg = 0
                
    def add_bat(self,num):
            self.bats = self.bats + num

    def add_run(self,num):
            self.runs = self.runs + num
            
    def add_hit(self,num):
            self.hits = self.hits + num
    def recalc_B_avg(self):
            self.batting_avg = float(self.hits/self.bats)
    def set_batting_avg(self, num):
            self.batting_avg = num
            
            
            
class output:
    # constructor:
    def __init__(self,players):
        self.players = players
            
    def output_season_stats(self):
        self.players = sorted(self.players, key=lambda v: v.batting_avg, reverse = True )
        for player in self.players:
            print "%s: %1.3f." % (player.name, round(player.batting_avg,3))
            



if len(sys.argv) < 2:
    sys.exit("Usage: %s filename" % sys.argv[0])
 
filename = sys.argv[1]
 
if not os.path.exists(filename):
    sys.exit("Error: File '%s' not found" % sys.argv[1])
else:
    prsr = parser(filename)
    prsr.parse()
    out = output(prsr.get_players())
    out.output_season_stats()
        

    