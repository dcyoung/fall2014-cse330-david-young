<!DOCTYPE html>

<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="style_sheet.css" />
        <script src="//cdn.sencha.io/ext-4.2.0-gpl/ext.js"></script>
        
        <script src="jquery.min.js"></script>
        <script src="jquery-ui.min.js"></script>
        
        <script type="text/javascript" src="login_script.js"></script>
        <script type="text/javascript" src="calendar_to_refer.js"></script>
        <script type="text/javascript" src="calendar_main_script.js"></script>
        
        <!--yet to implement below-->
        <!--<script type="text/javascript" src="addEvent.js"></script>-->
        
        <title>JS Calendar</title>
    </head>
    
    <body>
        
        <h2>Javascript Calendar</h2>
        <?php
            session_start();
            include_once('database.php');
        ?>
    
        <div id="login" class="login" >  
                <h3>Login</h3>
                <label>Username:</label>
                <input id="username" type="text" name="username" ><br/>
                
                <label>Password:</label>
                <input id="password" type="password" name="password"><br/>
                
                <button id="login_btn">login</button>
                <button id="signUp_btn">Sign up</button>
        </div>
        
        <div id="signUp" class="login" >
                <h3>Sign up</h3>    
        
                <label>Username:</label>
                <input id="reg_username" type="text" name="username" ><br/>
          
                <label>Password:</label>
                <input id="reg_password" type="password" name="password"><br/>
          
                <label>Email Address:</label>
                <input id="reg_email" type="text" name="email"><br/>
          
                <button id="reg_btn">register</button>
        </div>
          
        <div id="loginMessage" class="login"></div>
        
        <div class="login" id="welcome">
                <div id="usertag">Welcome</div>
                <button id="logout_btn">logout</button>
        </div>
        
                
        <div id="calendar">
            
                Below are the month's events.<br>
                
                <button id="prev_month_btn">Previous Month</button> 
                <span id="currentMonth"></span>
                <button id="next_month_btn">Next Month</button>
                
                <p id="testMessage"></p>
                
                <table id="calendarTable" class="calendar">
                    <tr class="week0">
                            <td class="day0"></td>
                            <td class="day1"></td>
                            <td class="day2"></td>
                            <td class="day3"></td>
                            <td class="day4"></td>
                            <td class="day5"></td>
                            <td class="day6"></td>
                    </tr>
                    <tr class="week1">
                            <td class="day0"></td>
                            <td class="day1"></td>
                            <td class="day2"></td>
                            <td class="day3"></td>
                            <td class="day4"></td>
                            <td class="day5"></td>
                            <td class="day6"></td>
                    </tr>
                    <tr class="week2">
                            <td class="day0"></td>
                            <td class="day1"></td>
                            <td class="day2"></td>
                            <td class="day3"></td>
                            <td class="day4"></td>
                            <td class="day5"></td>
                            <td class="day6"></td>
                    </tr>
                    <tr class="week3">
                            <td class="day0"></td>
                            <td class="day1"></td>
                            <td class="day2"></td>
                            <td class="day3"></td>
                            <td class="day4"></td>
                            <td class="day5"></td>
                            <td class="day6"></td>
                    </tr>
                    <tr class="week4">
                            <td class="day0"></td>
                            <td class="day1"></td>
                            <td class="day2"></td>
                            <td class="day3"></td>
                            <td class="day4"></td>
                            <td class="day5"></td>
                            <td class="day6"></td>
                    </tr>
            </table>
            
        </div>
        
        <div id="event" class="login">
                <div id="existingEvents"></div>
                
                <strong>Add a new Event</strong>
                <label>Event Title:</label>
                <input id="event_name" type="text" name="eventname" ></>
                
                <input type="hidden" id="day" name = "day" value="">
                <!--<label>Date:</label>-->
                <!--<input type="date" id ="date" name="date">-->
                
                <label>Time:</label>
                <select id="hour" name="hour">
                    <option selected="selected" value="0:00:00">0:00</option>
                    <option value="1:00:00">1:00</option>
                    <option value="2:00:00">2:00</option>
                    <option value="3:00:00">3:00</option>
                    <option value="4:00:00">4:00</option>
                    <option value="5:00:00">5:00</option>
                    <option value="6:00:00">6:00</option>
                    <option value="7:00:00">7:00</option>
                    <option value="8:00:00">8:00</option>
                    <option value="9:00:00">9:00</option>
                    <option value="10:00:00">10:00</option>
                    <option value="11:00:00">11:00</option>
                    <option value="12:00:00">12:00</option>
                    <option value="13:00:00">13:00</option>
                    <option value="14:00:00">14:00</option>
                    <option value="15:00:00">15:00</option>
                    <option value="16:00:00">16:00</option>
                    <option value="17:00:00">17:00</option>
                    <option value="18:00:00">18:00</option>
                    <option value="19:00:00">19:00</option>
                    <option value="20:00:00">20:00</option>
                    <option value="21:00:00">21:00</option>
                    <option value="22:00:00">22:00</option>
                    <option value="23:00:00">23:00</option>
                </select>
                <br>
                <input type="hidden" id="token" name="token" value="<?php echo $_SESSION['token'];?>" />
                
                <button id="new_event_btn">Create New Event</button>
                <button id="delete_event_btn">Delete</button>
                <button id="edit_event_btn">Edit</button>
                <button id="cancel_btn">Cancel</button>
        </div>
        
        <div id="edit-event-dialog-form" title="Confirm" style="display:none;">
            
            <!--Fetch and display events here-->
            <!--<br>-->
            <!--<button>Add new event</button>-->
        </div>
        
        </div>
        
        
        
        <script>
            //Hide elements immediately to avoid blinking on page-load
            $("#login").hide();
            $("#signUp").hide();
            $("#welcome").hide();
            $("#calendar").hide();
            $("#event").hide();
        </script>
    
    
    
    
    
    
    </body>
</html>
