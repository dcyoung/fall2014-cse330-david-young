var update = function(){
    
        document.getElementById("radio1").addEventListener("click", update, false);
        document.getElementById("radio2").addEventListener("click", update, false);
        document.getElementById("radio3").addEventListener("click", update, false);
        document.getElementById("radio4").addEventListener("click", update, false);
               
               
               
               
        //Get the input numbers by parsing the floats from the form values
        var input1 = parseFloat(document.forms.my_input_form.elements.input1.value);
        var input2 = parseFloat(document.forms.my_input_form.elements.input2.value);
        
        //Get the selected mathematical operation by getting the 
        var selected_math_op = getButton(document.forms.my_input_form.elements.operation);
        function getButton(math_operations) {
                for (var i = 0; i < math_operations.length; i++) {
                        var button = math_operations[i];
                        if (button.checked) {
                                return button;
                        }
                }
                return undefined;
        }
        
        var result = null;
        if ((input1 != null && input2 != null) && isFinite(String(input1)) && isFinite(String(input2))) {
                        
                switch(selected_math_op.value) {
                    case "add":
                        result = input1 + input2;
                        document.forms.my_input_form.elements.result.value = result;
                        break;
                    case "subtract":
                        result = input1 - input2;
                        document.forms.my_input_form.elements.result.value = result;
                        break;
                    case "multiply":
                        result = input1 * input2;
                        document.forms.my_input_form.elements.result.value = result;
                        break;
                    case "divide":
                        if (input2 == 0) {
                            document.forms.my_input_form.elements.result.value = "UNDEFINED"
                        }
                        else {
                            result = input1 / input2;
                            document.forms.my_input_form.elements.result.value = result;
                        }
                        break;
                                           
                }
                
        }
        else {
                document.forms.my_input_form.elements.result.value = "Null";
        }
                                 
}