document.addEventListener("DOMContentLoaded", fetchWeather, false);

var fetchWeather = function(){
    
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET",  "http://classes.engineering.wustl.edu/cse330/content/weather_json.php", true);
    
    xmlHttp.addEventListener("load", ajaxCallback, false);
    xmlHttp.send(null);
          
}

function ajaxCallback(event){
        var jsonData = JSON.parse(event.target.responseText);
        var humidity = jsonData.atmosphere.humidity;
        var temp = jsonData.current.temp;
        var city = jsonData.location.city;
        var state = jsonData.location.state;
        
        var tomorrow_picture_URL = "http://us.yimg.com/i/us/nws/weather/gr/" + jsonData.tomorrow.code + "ds.png";	
        var dayAfterTomorrow_picture_URL = "http://us.yimg.com/i/us/nws/weather/gr/" + jsonData.dayafter.code + "ds.png";
        document.getElementById("tomorrow_image").src = tomorrow_picture_URL;
        document.getElementById("dayaftertomorrow_image").src = dayAfterTomorrow_picture_URL;
        
        document.getElementById("location").innerHTML += "<strong>"+city+"</strong> "+state;
        document.getElementById("humidity").appendChild(document.createTextNode(humidity));
        document.getElementById("temp").appendChild(document.createTextNode(temp));
}
document.addEventListener("DOMContentLoaded", fetchWeather, false);